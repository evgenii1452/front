export const BACK_URLS = {
  clients: "/api/v1/clients",
  cars: "/api/v1/cars",
  departments: "/api/v1/departments",
  employees: "/api/v1/employees",
  orders: "/api/v1/orders",
  specifications: "/api/v1/specifications",
  menu: "/api/v1/menu"
}

