export const ENTITY_TITLES = {
  list: {
    clients: "Список клиентов",
    cars: "Список машин",
    departments: "Список отделов",
    orders: "Список заказов",
    employees: "Список сотрудников",
    specifications: "Список спецификаций",
  },
  create: {
    clients_create: "Создание клиента",
    cars_create: "Создание машины",
    departments_create: "Создание отдела",
    employees_create: "Создание сотрудника",
    orders_create: "Создание заказа",
    specifications_create: "Создание спецификации",
  },
  update: {
    clients_create: "Обновление клиента",
    cars_create: "Обновление машины",
    departments_create: "Обновление отдела",
    employees_create: "Обновление сотрудника",
    orders_create: "Обновление заказа",
    specifications_create: "Обновление спецификации",
  }
}

