import { axiosCreate } from '~/axios/axios'
import {BACK_URLS} from "~/configs/backUrls";

const axios = axiosCreate()

export default async function CarUpdate (id, data) {
  let url = `${BACK_URLS.cars}/${id}`;
  const response = await axios.patch(url, data).catch(function (error) {
    console.log(error.response)
    if (error.response) {
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
    }
  });

  return response.data
}
