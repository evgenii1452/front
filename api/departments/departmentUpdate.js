import { axiosCreate } from '~/axios/axios'
import {BACK_URLS} from "~/configs/backUrls";

const axios = axiosCreate()

export default async function DepartmentUpdate (id, data) {
  let url = `${BACK_URLS.departments}/${id}`;
  const response = await axios.patch(url, data).catch(function (error) {
    if (error.response) {
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
    }
  });

  return response.data
}
