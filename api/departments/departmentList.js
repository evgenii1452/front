import { axiosCreate } from '~/axios/axios'
import {BACK_URLS} from "~/configs/backUrls";

const axios = axiosCreate()

// Метод получения продуктов по фильтру

export default async function DepartmentList () {
  const response = await axios.get(BACK_URLS.departments).catch(function (error) {
    if (error.response) {
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
    }
  });

  return response.data
}
